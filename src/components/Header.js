import React from 'react';
import logo from '../images/logo.png';

const Header = () => {
  return (
    <div className='header-div'>
      <table className='header-table'>
        <tbody>
          <tr className='header-row'>
            <td className='header-icon'><img className='header-logo' src={logo} alt='funema' title='funema'/></td>
            <td className='header-text'><a className='header-name' href='#'>Canvas App</a></td>
            <td className='header-insert'></td>
          </tr>
        </tbody>
      </table>
    </div>
  )
};

export default Header;
