import React, { lazy, Suspense }  from 'react';
import Loader from './Loader';

const Header = lazy(() => import('./Header'));
// const Canvas = lazy(() => import('./Canvas'));
const ImageCanvas = lazy(() => import('./ImageCanvas'));

export default () => {
  return (
    <div className='dashboard'>
      <Suspense fallback={<Loader />}>
        <Header />
        <ImageCanvas />
      </Suspense>
    </div>
  )
};
