import React, { useEffect, useState, Fragment } from "react";
import { Stage, Layer, Image } from "react-konva";
import logo from '../images/logo.png';
import grommet from '../images/grommet.png';

export default function ImageCanvas(props) {
  let rectStyle;
  if(window.screen.width < 1000){
    rectStyle = {
      width : window.innerWidth - 20,
      height: props.height,
    };
  }else{
    rectStyle = {
      width : props.width,
      height: props.height,
    };
  };

  const [image, setImage] = useState(new window.Image());
  const [image2, setImage2] = useState(new window.Image());
  const [image3, setImage3] = useState(new window.Image());
  const [image4, setImage4] = useState(new window.Image());
  const [image5, setImage5] = useState(new window.Image());
  const [image6, setImage6] = useState(new window.Image());

  const [image7, setImage7] = useState(new window.Image());
  const [image8, setImage8] = useState(new window.Image());
  const [image9, setImage9] = useState(new window.Image());
  const [image10, setImage10] = useState(new window.Image());
  const [image11, setImage11] = useState(new window.Image());

  const [image12, setImage12] = useState(new window.Image());
  const [image13, setImage13] = useState(new window.Image());

  const [grommet1, setGrommet1] = useState(false);
  const [grommet2, setGrommet2] = useState(false);
  const [grommet3, setGrommet3] = useState(false);
  const [grommet4, setGrommet4] = useState(false);
  const [grommet5, setGrommet5] = useState(false);
  const [grommet6, setGrommet6] = useState(false);
  const [grommet7, setGrommet7] = useState(false);
  const [grommet8, setGrommet8] = useState(false);
  const [grommet9, setGrommet9] = useState(false);
  const [grommet10, setGrommet10] = useState(false);
  const [grommet11, setGrommet11] = useState(false);
  const [grommet12, setGrommet12] = useState(false);

  useEffect(() => {
    const img = new window.Image();
    const img2 = new window.Image();
    const img3 = new window.Image();
    const img4 = new window.Image();
    const img5 = new window.Image();
    const img6 = new window.Image();

    const img7 = new window.Image();
    const img8 = new window.Image();
    const img9 = new window.Image();
    const img10 = new window.Image();
    const img11 = new window.Image();

    const img12 = new window.Image();
    const img13 = new window.Image();

    img.src = logo;
    img2.src = grommet;
    img3.src = grommet;
    img4.src = grommet;
    img5.src = grommet;
    img6.src = grommet;

    img7.src = grommet;
    img8.src = grommet;
    img9.src = grommet;
    img10.src = grommet;
    img11.src = grommet;

    img12.src = grommet;
    img13.src = grommet;

    setImage(img);
    setImage2(img2);
    setImage3(img3);
    setImage4(img4);
    setImage5(img5);
    setImage6(img6);

    setImage7(img7);
    setImage8(img8);
    setImage9(img9);
    setImage10(img10);
    setImage11(img11);

    setImage12(img12);
    setImage13(img13);
  }, []);

const fourCorners = () => {
  setGrommet1(true);
  setGrommet2(false);
  setGrommet3(false);
  setGrommet4(false);
  setGrommet5(true);
  setGrommet6(false);
  setGrommet7(false);
  setGrommet8(true);
  setGrommet9(false);
  setGrommet10(false);
  setGrommet11(false);
  setGrommet12(true);
};

const eighteenTwentyFour = () => {
  setGrommet1(true);
  setGrommet2(true);
  setGrommet3(true);
  setGrommet4(true);
  setGrommet5(true);
  setGrommet6(true);
  setGrommet7(true);
  setGrommet8(true);
  setGrommet9(true);
  setGrommet10(true);
  setGrommet11(true);
  setGrommet12(true);
};

const topCorners = () => {
  setGrommet1(true);
  setGrommet2(false);
  setGrommet3(false);
  setGrommet4(false);
  setGrommet5(true);
  setGrommet6(false);
  setGrommet7(false);
  setGrommet8(false);
  setGrommet9(false);
  setGrommet10(false);
  setGrommet11(false);
  setGrommet12(false);
};

const none = () => {
  setGrommet1(false);
  setGrommet2(false);
  setGrommet3(false);
  setGrommet4(false);
  setGrommet5(false);
  setGrommet6(false);
  setGrommet7(false);
  setGrommet8(false);
  setGrommet9(false);
  setGrommet10(false);
  setGrommet11(false);
  setGrommet12(false);
};

const twelveEighteen = () => {
  setGrommet1(true);
  setGrommet2(false);
  setGrommet3(true);
  setGrommet4(false);
  setGrommet5(true);
  setGrommet6(true);
  setGrommet7(true);
  setGrommet8(true);
  setGrommet9(false);
  setGrommet10(true);
  setGrommet11(false);
  setGrommet12(true);
};

  useEffect(() => {
    
  }, [handleChange]);


  const handleChange = (e) => {
    const onChangeText = e.target.value;
    if(onChangeText == `corners4`){
      fourCorners();
    }else if(onChangeText == `feet18-24`){
      eighteenTwentyFour()
    }else if(onChangeText == `none`){
      none();
    }else if(onChangeText == `topcorner`){
      topCorners();
    }else if(onChangeText == `feet23`){
      fourCorners();
    }else if(onChangeText == `feet12-18`){
      twelveEighteen();
    }
  };

  return (
   <div className='canva-div'>
    <Stage width={window.innerWidth} height={510}>
      <Layer>
        <Image 
          x={9}
          y={0} 
          image={image}  
          width={rectStyle.width}
          height={rectStyle.height} 
          />


        {grommet1 && <Image 
          x={1}
          y={0} 
          image={image2}  
          />}
        {grommet3 && <Image 
        x={rectStyle.width/2}
        y={0} 
        image={image3}  
        />}
        {grommet2 && <Image 
        x={rectStyle.width/4}
        y={0} 
        image={image5}  
        />}
       {grommet4 &&  <Image 
        x={rectStyle.width/1.35}
        y={0} 
        image={image6}  
        />}
        {grommet5 && <Image 
        x={rectStyle.width}
        y={0} 
        image={image4}  
        />}


        {grommet12 && <Image 
          x={1}
          y={rectStyle.height - 12} 
          image={image7}  
        />}
        {grommet10 && <Image 
          x={rectStyle.width/2}
          y={rectStyle.height - 12} 
          image={image8}  
        />}
        {grommet8 && <Image 
          x={rectStyle.width}
          y={rectStyle.height - 12} 
          image={image9}  
        />}
        {grommet9 && <Image 
          x={rectStyle.width/4}
          y={rectStyle.height - 12} 
          image={image10}  
        />}
        {grommet11 && <Image 
          x={rectStyle.width/1.35}
          y={rectStyle.height - 12} 
          image={image11}  
        />}

        {grommet6 && <Image 
          x={1}
          y={rectStyle.height/2} 
          image={image12}  
        />}
        {grommet7 && <Image 
          x={rectStyle.width}
          y={rectStyle.height/2} 
          image={image13}  
        />}
      </Layer>
    </Stage>
    <Fragment>
      <form className='canva-form'>
        <select onChange={handleChange} className="canva-grommet-select" id='grommet-choose' type="text" 
          name="grommet">
            <option value='none'> None</option>
            <option value='feet23'>Every 2-3 Feet</option>
            <option value='feet18-24'> Every 18-24 Feet</option>
            <option value='feet12-18'> Every 12-18 Feet</option>
            <option value='corners4'> Four Corners</option>
            <option value='topcorner'> Top Corner</option>
        </select>
      </form>
    </Fragment>
   </div>
  );
};


ImageCanvas.defaultProps = {
  width : window.innerWidth/2,
  height: 500,
  imageArray : [],
}
