import React from 'react';
import loader from '../images/loader2.gif';

export default () => {
  return (
    <div className='loader-div'>
      <img src={loader} alt='funema' title='funema'/>
    </div>
  )
};
