const addNumbers = (a, b) => a + b;

test('should add two numbers', () => {
  const result = addNumbers(3, 4)

  if(result !== 7){
    throw new Error('Incorrect Result!');
  }
});
